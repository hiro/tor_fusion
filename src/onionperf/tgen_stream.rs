use anyhow::{Context, Error, Result};

use serde_json::Value;

#[derive(Debug, Default)]
pub struct TgenStream {
    pub stream_id: String,
    pub byte_info_payload_bytes_recv: i64,
    pub byte_info_payload_bytes_send: i64,
    pub byte_info_payload_progress_recv: String,
    pub byte_info_payload_progress_send: String,
    pub byte_info_total_bytes_recv: i64,
    pub byte_info_total_bytes_send: i64,
    pub elapsed_seconds_payload_bytes_recv: String,
    pub elapsed_seconds_payload_bytes_send: String,
    pub elapsed_seconds_payload_progress_recv: String,
    pub elapsed_seconds_payload_progress_send: String,
    pub is_complete: bool,
    pub is_error: bool,
    pub is_success: bool,
    pub stream_info_error: String,
    pub stream_info_id: i64,
    pub stream_info_name: String,
    pub stream_info_peername: String,
    pub stream_info_recvsize: i64,
    pub stream_info_sendsize: i64,
    pub stream_info_recvstate: String,
    pub stream_info_sendstate: String,
    pub stream_info_vertexid: String,
    pub time_info_created_ts: i64,
    pub time_info_now_ts: i64,
    pub time_info_usecs_to_checksum_recv: i64,
    pub time_info_usecs_to_checksum_send: i64,
    pub time_info_usecs_to_command: i64,
    pub time_info_usecs_to_first_byte_recv: i64,
    pub time_info_usecs_to_first_byte_send: i64,
    pub time_info_usecs_to_last_byte_recv: i64,
    pub time_info_usecs_to_last_byte_send: i64,
    pub time_info_usecs_to_proxy_choice: i64,
    pub time_info_usecs_to_proxy_init: i64,
    pub time_info_usecs_to_proxy_request: i64,
    pub time_info_usecs_to_proxy_response: i64,
    pub time_info_usecs_to_response: i64,
    pub time_info_usecs_to_socket_connect: i64,
    pub time_info_usecs_to_socket_create: i64,
    pub transport_info_error: String,
    pub transport_info_fd: i64,
    pub transport_info_local: String,
    pub transport_info_proxy: String,
    pub transport_info_remote: String,
    pub transport_info_state: String,
    pub unix_ts_start: f64,
    pub unix_ts_end: f64,
}

impl TgenStream {

    pub fn create(record: &Value, stream: &str) -> Result<TgenStream, Error> {
        let byte_info_payload_bytes_recv: i64 = if record["byte_info"]["payload-bytes-recv"].is_null() {
            -1
        } else {
            let bytes_recv = record["byte_info"]["payload-bytes-recv"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            bytes_recv.parse().with_context(|| format!("Failed to parse payload-bytes-recv: {}", bytes_recv))?
        };

        let byte_info_payload_bytes_send: i64 = if record["byte_info"]["payload-bytes-send"].is_null() {
            -1
        } else {
            let bytes_send = record["byte_info"]["payload-bytes-send"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            bytes_send.parse().with_context(|| format!("Failed to parse payload-bytes-send: {}", bytes_send))?
        };

        let byte_info_payload_progress_recv = record["byte_info"]["payload-progress-recv"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let byte_info_payload_progress_send = record["byte_info"]["payload-progress-send"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let byte_info_total_bytes_recv: i64 = if record["byte_info"]["total-bytes-recv"].is_null() {
            -1
        } else {
            let bytes_recv = record["byte_info"]["total-bytes-recv"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            bytes_recv.parse().with_context(|| format!("Failed to parse total-bytes-recv: {}", bytes_recv))?
        };

        let byte_info_total_bytes_send: i64 = if record["byte_info"]["total-bytes-send"].is_null() {
            -1
        } else {
            let bytes_send = record["byte_info"]["total-bytes-send"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            bytes_send.parse().with_context(|| format!("Failed to parse total-bytes-send: {}", bytes_send))?
        };

        let elapsed_seconds_payload_bytes_recv = record["elapsed_seconds"]["payload_bytes_recv"].to_string();

        let elapsed_seconds_payload_bytes_send = record["elapsed_seconds"]["payload_bytes_send"].to_string();

        let elapsed_seconds_payload_progress_recv = record["elapsed_seconds"]["payload_progress_recv"].to_string();

        let elapsed_seconds_payload_progress_send = record["elapsed_seconds"]["payload_progress_send"].to_string();

        let is_complete: bool = record["is_complete"].to_string()
            .parse()
            .with_context(|| "Failed to parse is_complete")?;

        let is_error: bool = record["is_error"].to_string()
            .parse()
            .with_context(|| "Failed to parse is_error")?;

        let is_success: bool = record["is_success"].to_string()
            .parse()
            .with_context(|| "Failed to parse is_success")?;

        let stream_info_error = record["stream_info"]["error"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let stream_info_id: i64 = if record["stream_info"]["id"].is_null() {
            -1
        } else {
            let id = record["stream_info"]["id"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            id.parse().with_context(|| format!("Failed to parse stream-info id: {}", id))?
        };

        let stream_info_name = record["stream_info"]["name"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let stream_info_peername = record["stream_info"]["peername"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let stream_info_recvsize: i64 = if record["stream_info"]["recvsize"].is_null() {
            -1
        } else {
            let recvsize = record["stream_info"]["recvsize"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            recvsize.parse().with_context(|| format!("Failed to parse stream-info recvsize: {}", recvsize))?
        };

        let stream_info_sendsize: i64 = if record["stream_info"]["sendsize"].is_null() {
            -1
        } else {
            let sendsize = record["stream_info"]["sendsize"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            sendsize.parse().with_context(|| format!("Failed to parse stream-info sendsize: {}", sendsize))?
        };

        let stream_info_recvstate = record["stream_info"]["recvstate"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let stream_info_sendstate = record["stream_info"]["sendstate"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let stream_info_vertexid = record["stream_info"]["vertexid"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let time_info_created_ts: i64 = if record["time_info"]["created_ts"].is_null() {
            -1
        } else {
            let created_ts = record["time_info"]["created-ts"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            created_ts.parse().with_context(|| format!("Failed to parse time_info created-ts: {}", created_ts))?
        };

        let time_info_now_ts: i64 = if record["time_info"]["now-ts"].is_null() {
            -1
        } else {
            let now_ts = record["time_info"]["now-ts"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            now_ts.parse().with_context(|| format!("Failed to parse time_info now_ts: {}", now_ts))?
        };

        let time_info_usecs_to_checksum_recv: i64 = if record["time_info"]["usecs-to-checksum-recv"].is_null() {
            -1
        } else {
            let checksum_recv = record["time_info"]["usecs-to-checksum-recv"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            checksum_recv.parse().with_context(|| format!("Failed to parse time_info usecs-to-checksum-recv: {}", checksum_recv))?
        };

        let time_info_usecs_to_checksum_send: i64 = if record["time_info"]["usecs-to-checksum-send"].is_null() {
            -1
        } else {
            let checksum_send = record["time_info"]["usecs-to-checksum-send"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            checksum_send.parse().with_context(|| format!("Failed to parse time_info usecs-to-checksum-send: {}", checksum_send))?
        };

        let time_info_usecs_to_command: i64 = if record["time_info"]["usecs-to-command"].is_null() {
            -1
        } else {
            let command = record["time_info"]["usecs-to-command"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            command.parse().with_context(|| format!("Failed to parse time_info usecs-to-command: {}", command))?
        };

        let time_info_usecs_to_first_byte_recv: i64 = if record["time_info"]["usecs-to-first-byte-recv"].is_null() {
            -1
        } else {
            let first_byte_recv = record["time_info"]["usecs-to-first-byte-recv"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            first_byte_recv.parse().with_context(|| format!("Failed to parse time_info usecs-to-first-byte-recv: {}", first_byte_recv))?
        };

        let time_info_usecs_to_first_byte_send: i64 = if record["time_info"]["usecs-to-first-byte-send"].is_null() {
            -1
        } else {
            let first_byte_send = record["time_info"]["usecs-to-first-byte-send"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            first_byte_send.parse().with_context(|| format!("Failed to parse time_info usecs-to-first-byte-send: {}", first_byte_send))?
        };

        let time_info_usecs_to_last_byte_recv: i64 = if record["time_info"]["usecs-to-last-byte-recv"].is_null() {
            -1
        } else {
            let last_byte_recv = record["time_info"]["usecs-to-last-byte-recv"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            last_byte_recv.parse().with_context(|| format!("Failed to parse time_info usecs-to-last-byte-recv: {}", last_byte_recv))?
        };

        let time_info_usecs_to_last_byte_send: i64 = if record["time_info"]["usecs-to-last-byte-send"].is_null() {
            -1
        } else {
            let last_byte_send = record["time_info"]["usecs-to-last-byte-send"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            last_byte_send.parse().with_context(|| format!("Failed to parse time_info usecs-to-last-byte-send: {}", last_byte_send))?
        };

        let time_info_usecs_to_proxy_choice: i64 = if record["time_info"]["usecs-to-proxy-choice"].is_null() {
            -1
        } else {
            let proxy_choice = record["time_info"]["usecs-to-proxy-choice"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            proxy_choice.parse().with_context(|| format!("Failed to parse time_info usecs-to-proxy-choice: {}", proxy_choice))?
        };

        let time_info_usecs_to_proxy_init: i64 = if record["time_info"]["usecs-to-proxy-init"].is_null() {
            -1
        } else {
            let proxy_init = record["time_info"]["usecs-to-proxy-init"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            proxy_init.parse().with_context(|| format!("Failed to parse time_info usecs-to-proxy-init: {}", proxy_init))?
        };

        let time_info_usecs_to_proxy_request: i64 = if record["time_info"]["usecs-to-proxy-request"].is_null() {
            -1
        } else {
            let proxy_request = record["time_info"]["usecs-to-proxy-request"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            proxy_request.parse().with_context(|| format!("Failed to parse time_info usecs-to-proxy-request: {}", proxy_request))?
        };

        let time_info_usecs_to_proxy_response: i64 = if record["time_info"]["usecs-to-proxy-response"].is_null() {
            -1
        } else {
            let proxy_response = record["time_info"]["usecs-to-proxy-response"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            proxy_response.parse().with_context(|| format!("Failed to parse time_info usecs-to-proxy-response: {}", proxy_response))?
        };

        let time_info_usecs_to_response: i64 = if record["time_info"]["usecs-to-response"].is_null() {
            -1
        } else {
            let response = record["time_info"]["usecs-to-response"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            response.parse().with_context(|| format!("Failed to parse time_info usecs-to-response: {}", response))?
        };

        let time_info_usecs_to_socket_connect: i64 = if record["time_info"]["usecs-to-socket-connect"].is_null() {
            -1
        } else {
            let socket_connect = record["time_info"]["usecs-to-socket-connect"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            socket_connect.parse().with_context(|| format!("Failed to parse time_info usecs-to-socket-connect: {}", socket_connect))?
        };

        let time_info_usecs_to_socket_create: i64 = if record["time_info"]["usecs-to-socket-create"].is_null() {
            -1
        } else {
            let socket_create = record["time_info"]["usecs-to-socket-create"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            socket_create.parse().with_context(|| format!("Failed to parse time_info usecs-to-socket-create: {}", socket_create))?
        };

        let transport_info_error = record["transport_info"]["error"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let transport_info_fd: i64 = if record["transport_info"]["fd"].is_null() {
            -1
        } else {
            let fd = record["transport_info"]["fd"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            fd.parse().with_context(|| format!("Failed to parse transport_info fd: {}", fd))?
        };

        let transport_info_local = record["transport_info"]["local"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let transport_info_proxy = record["transport_info"]["proxy"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let transport_info_remote = record["transport_info"]["remote"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let transport_info_state = record["transport_info"]["state"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let unix_ts_start: f64 = record["unix_ts_start"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse unix_ts_start")?;

        let unix_ts_end: f64 = record["unix_ts_end"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse unix_ts_end")?;

        let ts = TgenStream {
            stream_id: stream.to_string(),
            byte_info_payload_bytes_recv,
            byte_info_payload_bytes_send,
            byte_info_payload_progress_recv,
            byte_info_payload_progress_send,
            byte_info_total_bytes_recv,
            byte_info_total_bytes_send,
            elapsed_seconds_payload_bytes_recv,
            elapsed_seconds_payload_bytes_send,
            elapsed_seconds_payload_progress_recv,
            elapsed_seconds_payload_progress_send,
            is_complete,
            is_error,
            is_success,
            stream_info_error,
            stream_info_id,
            stream_info_name,
            stream_info_peername,
            stream_info_recvsize,
            stream_info_sendsize,
            stream_info_recvstate,
            stream_info_sendstate,
            stream_info_vertexid,
            time_info_created_ts,
            time_info_now_ts,
            time_info_usecs_to_checksum_recv,
            time_info_usecs_to_checksum_send,
            time_info_usecs_to_command,
            time_info_usecs_to_first_byte_recv,
            time_info_usecs_to_first_byte_send,
            time_info_usecs_to_last_byte_recv,
            time_info_usecs_to_last_byte_send,
            time_info_usecs_to_proxy_choice,
            time_info_usecs_to_proxy_init,
            time_info_usecs_to_proxy_request,
            time_info_usecs_to_proxy_response,
            time_info_usecs_to_response,
            time_info_usecs_to_socket_connect,
            time_info_usecs_to_socket_create,
            transport_info_error,
            transport_info_fd,
            transport_info_local,
            transport_info_proxy,
            transport_info_remote,
            transport_info_state,
            unix_ts_start,
            unix_ts_end
        };
        Ok(ts)
    }
}
