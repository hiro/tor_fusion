use anyhow::{Context, Error, Result};
use serde_json::Value;

#[derive(Debug, Default)]
pub struct TorCircuit {
    pub node_id: String,
    pub build_quantile: f64,
    pub build_timeout: i64,
    pub buildtime_seconds: f64,
    pub cbt_set: bool,
    pub circuit_id: i64,
    pub current_guards: String,
    pub elapsed_seconds: String,
    pub failure_reason_local: String,
    pub filtered_out: bool,
    pub path: String,
    pub unix_ts_end: f64,
    pub unix_ts_start: f64,
}

impl TorCircuit {

    pub fn create(record: &Value, node_id: &str) -> Result<TorCircuit, Error> {

        let build_quantile: f64 = if record["build_quantile"].is_null() {
            0.0
        } else {
            let bq = record["build_quantile"].to_string();
            bq.parse().with_context(|| format!("Failed to parse build_quantile: {}", bq))?
        };

        let build_timeout: i64 = if record["build_timeout"].is_null() {
            -1
        } else {
            let bt = record["build_timeout"].to_string();
            bt.parse().with_context(|| format!("Failed to parse build_timeout: {}", bt))?
        };

        let buildtime_seconds: f64 = if record["buildtime_seconds"].is_null() {
            0.0
        } else {
            let bs = record["buildtime_seconds"].to_string();
            bs.parse().with_context(|| format!("Failed to parse buildtime_seconds: {}", bs))?
        };

        let cbt_set: bool = if record["cbt_set"].is_null() {
            false
        } else {
            let cbt = record["cbt_set"].to_string();
            cbt.parse().with_context(|| "Failed to parse cbt_set")?
        };

        let circuit_id: i64 = record["circuit_id"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse circuit_id")?;

        let current_guards = record["current_guards"].to_string();
        let elapsed_seconds = record["elapsed_seconds"].to_string();
        let failure_reason_local = record["failure_reason_local"].to_string();

        let filtered_out: bool = record["filtered_out"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse filtered_out")?;

        let path = record["path"].to_string();

        let unix_ts_end: f64 = record["unix_ts_end"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse unix_ts_end")?;

        let unix_ts_start: f64 = record["unix_ts_start"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse unix_ts_start")?;

        let tc = TorCircuit {
            node_id: node_id.to_string(),
            build_quantile,
            build_timeout,
            buildtime_seconds,
            cbt_set,
            circuit_id,
            current_guards,
            elapsed_seconds,
            failure_reason_local,
            filtered_out,
            path,
            unix_ts_end,
            unix_ts_start,
        };

        Ok(tc)
    }
}
