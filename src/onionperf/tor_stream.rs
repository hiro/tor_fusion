use anyhow::{Context, Error, Result};
use serde_json::Value;

#[derive(Debug, Default)]
pub struct TorStream {
    pub stream_id: String,
    pub circuit_id: String,
    pub elapsed_seconds: String,
    pub source: String,
    pub target: String,
    pub unix_ts_end: f64,
    pub unix_ts_start: f64,
}

impl TorStream {

    pub fn create(record: &Value) -> Result<TorStream, Error> {
        let stream_id = record["stream_id"].to_string();

        let circuit_id = record["circuit_id"]
            .as_str()
            .map(|s| s.trim()).unwrap_or("").to_string();

        let elapsed_seconds = record["elapsed_seconds"].to_string();

        let source = record["source"]
            .as_str()
            .map(|s| s.trim()).unwrap_or("").to_string();

        let target = record["target"]
            .as_str()
            .map(|s| s.trim()).unwrap_or("").to_string();

        let unix_ts_end: f64 = record["unix_ts_end"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse unix_ts_end")?;

        let unix_ts_start: f64 = record["unix_ts_start"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse unix_ts_start")?;

        let ts = TorStream {
            stream_id,
            circuit_id,
            elapsed_seconds,
            source,
            target,
            unix_ts_end,
            unix_ts_start,
        };

        Ok(ts)
    }
}