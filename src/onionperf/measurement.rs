use anyhow::{Context, Error, Result};
use serde_json::Value;

#[derive(Debug, Default)]
pub struct Measurement {
    pub stream_id: String,
    pub tor_stream_id: String,
    pub circuit_id: String,
    pub filesize_bytes: i32,
    pub payload_progress_100: f64,
    pub payload_progress_80: f64,
    pub time_to_first_byte: i64,
    pub time_to_last_byte: i64,
    pub error_code: String,
    pub endpoint_local: String,
    pub unix_ts_end: f64,
    pub unix_ts_start: f64,
    pub mbps: f64,
    pub path: String,
    pub cbt_set: bool,
    pub filtered_out: bool,
    pub is_onion: bool,
    pub middle: String,
    pub guard: String,
    pub exit: String,
}

impl Measurement {

    pub fn create(record: &Value, node_id: &str, stream: &str, tor_stream_id: &str, circuit_id: &str) -> Result<Measurement, Error> {
        let stream_json = record["data"][node_id]["tgen"]["streams"][stream].clone();
        let tor_stream = &record["data"][node_id]["tor"]["streams"][tor_stream_id];
        let target = tor_stream["target"].to_string();
        let circuit_json = &record["data"][node_id]["tor"]["circuits"][&circuit_id];
        let path = &circuit_json["path"];

        let cbt_set: bool = if circuit_json["cbt_set"].is_null() {
            false
        } else {
            let cbt = circuit_json["cbt_set"].to_string();
            cbt.parse().with_context(|| format!("Failed to parse cbt_set {}", cbt))?
        };

        let filtered_out: bool = if circuit_json["filtered_out"].is_null() {
            false
        } else {
            let filtered = circuit_json["filtered_out"].to_string();
            filtered.parse().with_context(|| format!("Failed to parse filtered_out {}", filtered))?
        };

        let is_onion = target.contains(".onion");

        let filesize_bytes: i32 = if stream_json["stream_info"]["recvsize"].is_null() {
            -1
        } else {
            let bytes = stream_json["stream_info"]["recvsize"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            bytes.parse().with_context(|| format!("Failed to parse stream-info recvsize: {}", bytes))?

        };
        let payload_progress_100: f64 = if stream_json["elapsed_seconds"]["payload_progress_recv"].is_null() ||
            stream_json["elapsed_seconds"]["payload_progress_recv"]["1.0"].is_null() {
            -1.0
        } else {
            stream_json["elapsed_seconds"]["payload_progress_recv"]["1.0"]
                .to_string()
                .parse()
                .with_context(|| "Failed to parse payload-progress-recv-100")?

        };

        let payload_progress_80: f64 = if stream_json["elapsed_seconds"]["payload_progress_recv"].is_null() ||
            stream_json["elapsed_seconds"]["payload_progress_recv"]["0.8"].is_null() {
            -1.0
        } else {
            stream_json["elapsed_seconds"]["payload_progress_recv"]["0.8"]
                .to_string()
                .parse()
                .with_context(|| "Failed to parse payload-progress-recv-80")?
        };

        let time_to_first_byte: i64 = if stream_json["time_info"]["usecs-to-first-byte-recv"].is_null() {
            -1
        } else {
            let first_byte_recv = stream_json["time_info"]["usecs-to-first-byte-recv"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            first_byte_recv.parse().with_context(|| format!("Failed to parse time_info usecs-to-first-byte-recv: {}", first_byte_recv))?
        };

        let time_to_last_byte: i64 = if stream_json["time_info"]["usecs-to-last-byte-recv"].is_null() {
            -1
        } else {
            let last_byte_recv = stream_json["time_info"]["usecs-to-last-byte-recv"]
                .as_str()
                .map(|s| s.trim())
                .unwrap_or("");
            last_byte_recv.parse().with_context(|| format!("Failed to parse time_info usecs-to-last-byte-recv: {}", last_byte_recv))?
        };

        let error_code = stream_json["transport_info"]["error"]
            .as_str()
            .map(|s| s.trim()).unwrap().to_string();

        let endpoint_local= stream_json["transport_info"]["local"]
                .as_str()
                .map(|s| s.trim()).unwrap().to_string();

        let unix_ts_end: f64 = stream_json["unix_ts_end"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse unix_ts_end")?;

        let unix_ts_start: f64 = stream_json["unix_ts_start"]
            .to_string()
            .parse()
            .with_context(|| "Failed to parse unix_ts_start")?;

        // Calculate mbps, handling division by zero
        // Explanation of the math below for computing Mbps:

        // For 5 MiB downloads we extract the number of seconds that have elapsed between
        // receiving bytes 4,194,304 and 5,242,880, which is a total amount of 1,048,576
        // bytes or 8,388,608 bits or 8.388608 megabits. We want the reciprocal of
        // that value with unit megabits per second.

        let mbps = if payload_progress_100 - payload_progress_80 != 0.0 {
            8.388608 / (payload_progress_100 - payload_progress_80)
        } else {
            // Handle division by zero.
            // 8.388608 are the megabits transferred between 80% and 100%:
            // (5242880 Bytes - 4194304 Bytes) * 8 Bytes/bit / (1024 * 1024 Bytes) = 8.388608 megabits
            8.388608
        };

        let mut guard = String::new();
        let mut exit = String::new();
        let mut middle_vec = Vec::new();


        if let Some(array) = path.as_array() {
            for (index, node) in array.iter().enumerate() {
                // Example use of index
                if index == 0 {
                    // First element
                    let raw_guard = node[0].as_str().unwrap_or_default().to_string();
                    guard = raw_guard.trim_start_matches('$')
                        .split('~').next().unwrap_or_default().to_string().parse()?;
                } else if index == array.len() - 1 {
                    // Last element
                    let raw_exit = node[0].as_str().unwrap_or_default().to_string();
                    exit = raw_exit.trim_start_matches('$')
                        .split('~').next().unwrap_or_default().to_string().parse()?;
                } else {
                    // Middle elements
                    let raw_middle = node[0].as_str().unwrap_or_default().to_string();
                    let middle_node = raw_middle.trim_start_matches('$').to_string();
                    middle_vec.push(middle_node);
                }
            }
        }
        let middle = middle_vec
            .join(", ")
            .split('~')
            .nth(0)
            .unwrap_or(&middle_vec.join(", "))
            .to_string();


        let m = Measurement {
            stream_id: stream.to_string(),
            tor_stream_id: tor_stream_id.to_string(),
            circuit_id: circuit_id.to_string(),
            filesize_bytes,
            payload_progress_100,
            payload_progress_80,
            time_to_first_byte,
            time_to_last_byte,
            error_code,
            endpoint_local,
            unix_ts_end,
            unix_ts_start,
            mbps,
            path: path.to_string(),
            cbt_set,
            filtered_out,
            is_onion,
            middle,
            guard,
            exit,
        };

        Ok(m)
    }
}