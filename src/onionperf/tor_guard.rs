use anyhow::{Context, Error, Result};
use serde_json::Value;

#[derive(Debug, Default)]
pub struct TorGuard {
    pub country: String,
    pub dropped_ts: f64,
    pub fingerprint: String,
    pub nickname: String,
}

impl TorGuard {
    pub fn create(record: &Value) -> Result<TorGuard, Error> {
        let country = record["country"].to_string();
        let fingerprint = record["fingerprint"].to_string();
        let nickname = record["nickname"].to_string();

        let dropped_ts: f64 = if record["dropped_ts"].is_null() {
           -1.0
        } else {
            let ts = record["dropped_ts"].to_string();
            ts.parse().with_context(|| "Failed to parse dropped_ts")?
        };

        let tg = TorGuard {
            country,
            dropped_ts,
            fingerprint,
            nickname,
        };

        Ok(tg)
    }
}