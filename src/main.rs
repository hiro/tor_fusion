mod cli;
mod onionperf;

use anyhow::Result;
use clap::Parser;

use crate::cli::Cli;

fn main() -> Result<()> {

    let args = Cli::parse();
    let path = &args.path;

    let _ = onionperf::run(path);

    Ok(())

}
